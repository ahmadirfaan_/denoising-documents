import cv2
import time

def remove_noise_otsu(image):
    start = time.time()
    ret, result = cv2.threshold(image,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    end = time.time()
    print("Time for Otsu Thresholding: {} s".format(end-start))
    return ret, result