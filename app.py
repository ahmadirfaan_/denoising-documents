from flask import Flask, request, jsonify, send_file
import os
import socket
import cv2
import numpy as np
from remove_noise_otsu import remove_noise_otsu
from remove_noise_gaussianBlur import remove_noise_gaussianBlur
import zipfile
import pytesseract

#Additional Resource: https://stackoverflow.com/questions/28568687/send-with-multiple-csvs-using-flask

app = Flask(__name__)

@app.route("/denoise", methods=['POST'])
def denoise():
    
    if('file' not in request.files):
       return jsonify({'msg': 'Check your POST request!'})

    filestr = request.files['file'].read()

    image_name = request.files['file'].filename.split(".")[0]
    print(image_name)

    npimg = np.fromstring(filestr, np.uint8)

    img = cv2.imdecode(npimg, cv2.IMREAD_GRAYSCALE)

    thres, otsu_result = remove_noise_otsu(img)
    bg, gaussian_result = remove_noise_gaussianBlur(img)

    otsu_filename = "output/{}_otsu.jpg".format(image_name)
    otsu_textname = "output/{}_text_otsu.txt".format(image_name)

    gauss_filename = "output/{}_gauss.jpg".format(image_name)
    gauss_textname = "output/{}_text_gauss.txt".format(image_name)

    zip_filename = '{}.zip'.format(image_name)

    os.makedirs("output/", mode=0o777, exist_ok=True)

    # print()
    text_otsu = pytesseract.image_to_string(otsu_result).split("\n")
    with open(otsu_textname, 'w+') as f:
        for row in text_otsu:
            if(row.strip() != ""):
                f.write(row)
                f.write('\n')
    
    text_gauss = pytesseract.image_to_string(gaussian_result).split("\n")
    with open(gauss_textname, 'w+') as f:
        for row in text_gauss:
            if(row.strip() != ""):
                f.write(row)
                f.write('\n')
            
    # print(pytesseract.image_to_string(gaussian_result))

    if(not cv2.imwrite(otsu_filename, otsu_result)):
        print("Saving Otsu image Failed")
    if(not cv2.imwrite(gauss_filename, gaussian_result)):
        print("Saving Gaussian image Failed")

    @app.after_request
    def delete(response):
        os.remove(otsu_filename)
        os.remove(gauss_filename)
        os.remove(zip_filename)
        os.remove(otsu_textname)
        os.remove(gauss_textname)
        os.rmdir("output/")
        return response

    zip_output(image_name, zip_filename)

    return send_file('{}.zip'.format(image_name),
            mimetype = 'zip',
            attachment_filename= '{}.zip'.format(image_name),
            as_attachment = True)

def zip_output(image_name, zip_filename):

    zipf = zipfile.ZipFile(zip_filename,'w', zipfile.ZIP_DEFLATED)
    for root,dirs, files in os.walk('output/'):
        for file in files:
            zipf.write('output/'+file)
    zipf.close()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
