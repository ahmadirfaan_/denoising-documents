import time
import cv2
import numpy as np

def remove_noise_gaussianBlur(image):
    start = time.time()
    bg = cv2.GaussianBlur(image,(13,13),0)
    mask = image < (bg - 35)
    result = np.where(mask, image, 255)
    kernel = np.ones((2,2),np.uint8)
    erosion = cv2.erode(result,kernel,iterations = 1)
    end = time.time()
    print("Time for Gaussian Blur: {} s".format(end-start))
    return bg, erosion