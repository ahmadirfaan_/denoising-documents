# denoising-documents

A flask project with Docker to serve an API that needs an image in a POST request and returns a zip which contains: 
- Otsu Thresholding result on the image ({image_name}_otsu.jpg)
- Noise removal using Gaussian Blur on the image ({image_name}_gauss.jpg)
- Text extracted using Pytesseract Library ({image_name}\_text_(otsu/gauss).txt)

### HOW TO USE

Clone the project into your local

```
git clone https://gitlab.com/ahmadirfaan_/denoising-documents.git
cd denoising-documents
```

Build the image (you can change the _denoise-ndflx_ part)
```
docker build -t denoise-ndflx .
```

Run the docker (if you change the image name before, don't forget to change the image name here)
```
docker run -it --rm -p 80:80 denoise_ndflx
```

### HOW TO REQUEST 

You should request a POST into **ipaddress**/denoise using

| Parameter |    Type   |
| ------    | ------    |
| file      | jpg/png   |

Input example: 

![example](/uploads/c1697a28198fee9e9e6f8e5f7d3d1744/example.png)

On Postman: 

![Screenshot_from_2021-09-10_13-12-52](/uploads/bd82c518d39edc3ae13bd1cd53e79e4d/Screenshot_from_2021-09-10_13-12-52.png)

Output zip: 

![Screenshot_from_2021-09-10_13-13-54](/uploads/adb3a42319b2f1063bb34f0e2446cace/Screenshot_from_2021-09-10_13-13-54.png)

Image result: 

Otsu Thresholding Result:

![example_otsu](/uploads/10cb05de31eb61130d4a40355eaebcd5/example_otsu.jpg)

Gaussian Blur Result:

![example_gauss](/uploads/7e376faac27cb697b48c98b54d2c5888/example_gauss.jpg)
